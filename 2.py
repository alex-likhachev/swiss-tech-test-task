﻿
class A(object):
    def __init__(self, value):
        self._value = value
    def foo(self):
        self.bar()
        return self._value

def call_foo(inst):
    try:
        assert isinstance(inst, A)
        if inst.foo() < 10:
            return True
        else:
            return False
    except AttributeError:
        print('attribute error')
        return False
    finally:
        return False

class B(A):
    def bar(self):
        pass

print(call_foo(B(5)))






