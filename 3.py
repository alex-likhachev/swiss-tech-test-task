﻿
from threading import Thread, Lock
import time
import random

class LeakyBucket:

    def __init__(self, capacity, leak_rate):
        self._capacity = capacity
        self._leak_rate = leak_rate
        self._packets = []
        self.__lock = Lock()
        self._leak_thread = Thread(target=self.leak_packet, args=[])
        self._leak_thread.start()

    def add_packet(self, packet):
        self.__lock.acquire()
        print('trying to add packet: ' + str(packet))
        if len(self._packets) >= self._capacity:
            print('no space for packets, ignore: ' + str(packet))
            self.__lock.release()
            return
        self._packets.append(packet)
        print('added packet: ' + str(packet))
        self.__lock.release()

    def leak_packet(self):
        while True:
            self.__lock.acquire()
            print('current packets: ' + str(self._packets))
            if len(self._packets) > 0:
                print('!!! packet leaked: ' + str(self._packets[0]))
                self._packets.pop(0)
            self.__lock.release()
            time.sleep(self._leak_rate)

bucket = LeakyBucket(2, 1.0)

def add_packet_to_bucket():
    while True:
        bucket.add_packet(random.randint(0, 201))
        #time.sleep(random.uniform(1.0, 2.5))
        time.sleep(1.0)

add_packet_thread_0 = Thread(target=add_packet_to_bucket)
add_packet_thread_0.start()
add_packet_thread_1 = Thread(target=add_packet_to_bucket)
add_packet_thread_1.start()
add_packet_thread_2 = Thread(target=add_packet_to_bucket)
add_packet_thread_2.start()


















































